#include <stdio.h>
#include <math.h>

float squareRoot(float *number)
{
    if (*number > 0)
    {
        *number = sqrt(*number);
        return 1;
    }
    else
    {
        return 0;
    }
}
//==============================================================================
int main(int argc, char** argv)
{
    float user;

    printf("Enter a float number: ");
    scanf("%f", &user);

    if (squareRoot(&user) == 1)
    {
        printf("%.2f\n", user);
    }
    else
    {
        printf("\nIt is not possible to calculate the square root of a negative number.\n");
    }

    return 1;
}
