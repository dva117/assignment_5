#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define ARRSIZE 100

char* find_third_word(char *arr)
{
    int i = 0;
    int space = 0;
    if (strlen(arr) == 0)
    {
        return 0;
    }
    while (isspace(arr[i]))     //ignores spaces at the begining
    {
        i++;
    }
    while (arr[i] != 0)
    {
        if(space >= 2)      //when it has found the third word
        {
            return &arr[i];
        }
        while (isalpha(arr[i]))
        {
            i++;
        }
        if (isspace(arr[i]))
        {
            while(isspace(arr[i]))     //skips multiple spaces
            {
                i++;
            }
            space++;
        }    
    }
    return 0;
}
//==============================================================================
int main(int argc, char** argv)
{
    char user[ARRSIZE];
    char* third_word;
    int run = 1;

    while (run)
    {
        user[0] = 0;
        third_word = 0;
        printf("\nEnter a sentence: ");
        gets(user);

        third_word = find_third_word(user);
        if(third_word != 0)
        {
            printf("%s", third_word);
        }
        else
        {
            printf("\nThe sentence is too short\n");
        }
        printf("\nDo you wish to run the program again? (1 for yes, 0 for no): ");
        scanf("%d", &run);
        fflush(stdin);
    }
    return 1;
}
