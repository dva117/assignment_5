#include <stdio.h>
#include <string.h>

#define ARRSIZE 100
#define THOUSANDONE 1001


void most_frequent_number(int *numbers, int *occurance, int input_amount, int *frequence, int *the_frequent_number)
{
    int i;

    for (i = 0; i < input_amount; i++)
    {
        occurance[numbers[i]] += 1;           //the value of numbers[i] is used as the position for occurance where we store the numbers frequency.
    }

    for (i = 0; i < THOUSANDONE; i++)
    {
        if(occurance[i] > *frequence)
        {
            *frequence = occurance[i];
            *the_frequent_number = i;
        }
    }
}
//==============================================================================
int main(int argc, char** argv)
{
    int numbers[ARRSIZE] = {0};
    int occurance[THOUSANDONE] = {0};
    int frequence;
    int the_frequent_number;
    int input_amount;
    int run = 1;

    while(run)
    {
        memset(numbers, 0, sizeof(numbers));
        memset(occurance, 0, sizeof(occurance));
        frequence = 0;
        the_frequent_number = 0;
        input_amount = 0;

        printf("Enter numbers between 0 and 1000 (max 100 numbers). End with a negative number.\n\n");

        while(input_amount < 100)
        {
            printf("Number: ");
            scanf("%d", &numbers[input_amount]);

            if ((numbers[input_amount] < 0) || (numbers[input_amount] > 1000))
            {
                break;
            }
            input_amount ++;
        }  
        printf("\nYou entered a number which was not within the 0-1000 range or the maximum of 100 numbers and exited.");

        most_frequent_number(numbers, occurance, input_amount, &frequence, &the_frequent_number);

        printf("\nThe number %d occurs the most times, a total of %d times.\n", the_frequent_number, frequence);

        printf("\nOne more time? (1 for yes, 0 for no): ");
        scanf("%d", &run);
        fflush(stdin);
    }
    return 0;
}
